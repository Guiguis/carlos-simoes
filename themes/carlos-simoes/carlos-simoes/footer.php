<!-- Footer -->
<section class="section-padding footer bg-white">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-3">
				<h4>
					<a class="text-link logo" href="/index.php">
						<span class="menu-icon">
							<i class="fas fa-map-marker-alt"></i>
						</span>
						<strong class="text-title">Carlos</strong> Simões
					</a>
				</h4>
				
				<?php  
					$whatsapp = get_field('whatsapp', 'option'); 
					$email    = get_field('email', 'option'); 
				?>

				<p class="mb-0">
					<i class="mdi mdi-whatsapp whatsapp-icon"></i>
					<a class="text-link" href="" title="Whatsapp">
						<?= $whatsapp; ?>
					</a>
				</p>
				<p class="mb-0">
					<i class="mdi mdi-email email-icon"></i>
					<a class="text-link" href="mailto:<?= $email; ?>" title="E-mail">
						<?= $email; ?>
					</a>
				</p>
			</div>
			<div class="col-lg-2 col-md-2">
				<h6 class="mb-4">MENU</h6>
				<ul>
					<li><a href="/index.php">Home</a></li>
					<li><a href="/imovel">Imóveis</a></li>
				</ul>
			</div>
			<div class="col-lg-2 col-md-2">
				<h6 class="mb-4 invisible d-none d-sm-block d-sm-none d-md-block">sa</h6>
				<ul>
					<li><a href="/sobre">Sobre</a></li>
					<li><a href="/contato">Contato</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4">
				<h6 class="mb-4 text-uppercase">Nossas redes sociais</h6>
				
				<?php  
					$instagram = get_field('instagram', 'option'); 
					$linkedin  = get_field('linkedin', 'option'); 
				?>
				
				<div class="footer-social">
					<a href="mailto:<?= $email; ?>" target="_blank" title="E-mail">
						<i class="mdi mdi-email"></i>
					</a>
					<a href="<?= $instagram; ?>" target="_blank" title="Instagram">
						<i class="mdi mdi-instagram"></i>
					</a>
					<a href="<?= $linkedin; ?>" target="_blank" title="LinkedIn">
						<i class="mdi mdi-linkedin"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Footer -->

<!-- Copyright -->
<section class="pt-4 pb-4 text-center">
	<p class="mt-0 mb-0">&copy; Copyright <?php echo date("Y"); ?> <br/> Guilherme Miranda</p>
</section>
<!-- End Copyright -->

<!-- JQUERY AND BOOTSTRAP BUNDLE -->
<?php wp_enqueue_script('jquery-min', 
	  VENDOR_THEME . 'jquery/jquery.min.js',  
	  array(), false, true) ?>

<?php wp_enqueue_script('bootstrap-bundle', 
      VENDOR_THEME . 'bootstrap/js/bootstrap.min.js',  
      array('jquery-min'), false, true) ?>

<?php wp_footer(); ?>
</html>