<!-- MAIN STYLE -->
<?php wp_enqueue_style( 'carousel', STYLES_THEME . 'carousel.css', false, '1.1', 'all'); ?>
	

<?php 

	$imoveis_carousel = Imoveis_Public::get_imoveis_carousel();

	$whatsapp    = get_field('whatsapp', 'option'); 
	$email       = get_field('email', 'option'); 
	$titulo 	 = get_field('titulo', 'option'); 
	$creci 	 	 = get_field('creci', 'option'); 
	$descricao_geral = get_field('descricao_geral', 'option'); 
	$imagem 		 = get_field('imagem', 'option'); 
	$imagem_slide    = get_field('imagem_slide', 'option'); 
?>
<!-- Main Slider With Form -->
<?php if(!empty($imoveis_carousel['posts'])): ?>
<section class="imoveis-slider slider-h-auto">
	<div id="imovelslider" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<?php for($i = 0; $i < count($imoveis_carousel['posts']); $i++): ?>
                <li data-target="#imovelslider" 
                    data-slide-to="<?= $i; ?>" 
                    class="<?php if($i == 0) echo 'active'; ?>"></li>
            <?php endfor; ?>
            <li data-target="#imovelslider" 
                data-slide-to="<?= count($imoveis_carousel['posts']); ?>"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<?php $i = 0; foreach ($imoveis_carousel['posts'] as $imovel): ?> 
				<div class="carousel-item carousel-item-background <?php if($i == 0): echo 'active'; endif; ?>" 
				     style="background-image: url('<?= $imovel['imagem']['url']; ?>')">
					<div class="overlay"></div>
					<div class="section-padding">
						<div class="container banner-list pl-5 pr-5">
							<div class="row">
								<div class="col-lg-8 col-md-8 order-2 order-md-1">
									<h1 class="mt-5 mb-4 text-white">
										<?= $imovel['title'];?>
									</h1>
									<h6 class="mb-5 text-white text-description">
										<?= $imovel['descricao_slide'] ?>
									</h6>
									<a href="/contato">
										<button class="btn btn-main primary-color" type="button">
											Contato
										</button>
									</a>
									<a href="<?= $imovel['url'] ?>">
										<button class="btn btn-outline-main primary-color" type="button">
											Ver Imóvel
										</button>
									</a>
								</div>
								<div class="col-lg-4 col-md-4 order-1 order-md-2">
									<div class="card card-list mb-0 box-shadow-none">
										<a href="<?= $imovel['url'] ?>">
											<span class="badge badge-info">
												<?= $imovel['status']->name ?>
											</span>
											<img class="card-img-top" 
												 src="<?= $imovel['imagem']['url'];?>" 
												 alt="<?= $imovel['imagem']['alt'];?>">
											<div class="card-body">
												<h5 class="card-title">
													<?= $imovel['title'];?>
												</h5>
												<h6 class="card-subtitle mb-2 text-muted">
													<i class="mdi mdi-home-map-marker"></i> 
													<?= $imovel['cidade'];?>
												</h6>
												<h3 class="text-success mb-0 mt-3">
													<?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                        <?php if($imovel['valor'] > 0): echo 'R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
												</h3>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php $i++; endforeach; ?>
			<div class="carousel-item carousel-item-background <?php if($i == 0): echo 'active'; endif; ?>" 
				     style="background-image: url('<?= $imagem_slide['url']; ?>')">
					<div class="overlay"></div>
					<div class="section-padding">
						<div class="container banner-list pl-5 pr-5">
							<div class="row">
								<div class="col-lg-8 col-md-8 order-2 order-md-1">
									<h1 class="mt-5 mb-4 text-white">
										<?= $titulo; ?> <br/>
									</h1>
									<h6 class="mb-5 text-white text-description">
										<?= $descricao_geral; ?>
									</h6>
									<a href="/contato"> 
										<button class="btn btn-main primary-color" type="button">
											Entre em Contato
										</button>
									</a>
									<a href="/imovel"> 
										<button class="btn btn-outline-main primary-color" type="button">
											Imóveis
										</button>
									</a>
								</div>
								<div class="col-lg-4 col-md-4 order-1 order-md-2">
									<div class="card card-list mb-0 box-shadow-none">
										<a href="#">
											<span class="badge badge-carlos">
												#Carlos Simões
											</span>
											<img class="card-img-top" 
												 style="border: 4px solid #ddd;"
												 src="<?= $imagem['url']; ?>"/>
											<div class="card-body">
												<h5 class="card-title">
													<?= $titulo;  ?>
												</h5>
												<p class="mb-0">
													<a class="text-primary-color">
														CRECI: <?= $creci; ?>
													</a>
												</p><p class="mb-0">
													<a class="text-primary-color">
														<?= $whatsapp ?>
													</a>
												</p>
												<p class="mb-0">
													<a class="text-link" href="mailto:<?= $email; ?>">
														<?= $email; ?>
													</a>
												</p>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#imovelslider" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Anterior</span>
		</a>
		<a class="carousel-control-next" href="#imovelslider" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Próximo</span>
		</a>
	</div>
</section>
<!-- End Main Slider With Form -->
<?php endif; ?>

<!-- Main Slider With Form -->
<?php if(empty($imoveis_carousel['posts'])): ?>
<section class="imoveis-slider slider-h-auto">
	<div id="imovelslider" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="carousel-item carousel-item-background <?php if($i == 0): echo 'active'; endif; ?>" 
			     style="background-image: url('<?= $imagem_slide['url']; ?>')">
				<div class="overlay"></div>
				<div class="section-padding">
					<div class="container banner-list pl-5 pr-5">
						<div class="row">
							<div class="col-lg-8 col-md-8 order-2 order-md-1">
								<h1 class="mt-5 mb-4 text-white">
									<?= $titulo; ?> <br/>
								</h1>
								<h6 class="mb-5 text-white text-description">
									<?= $descricao_geral; ?>
								</h6>
								<a href="/contato"> 
									<button class="btn btn-main primary-color" type="button">
										Entre em Contato
									</button>
								</a>
								<a href="/imovel"> 
									<button class="btn btn-outline-main primary-color" type="button">
										Imóveis
									</button>
								</a>
							</div>
							<div class="col-lg-4 col-md-4 order-1 order-md-2">
								<div class="card card-list mb-0 box-shadow-none">
									<a href="#">
										<span class="badge badge-info">
											#Carlos Simões
										</span>
										<img class="card-img-top" 
												 style="border: 4px solid #ddd;"
												 src="<?= $imagem['url']; ?>"/>
										<div class="card-body">
											<h5 class="card-title">
												<?= $titulo;  ?>
											</h5>
											<p class="mb-0">
												<a class="text-primary-color">
													CRECI: <?= $creci; ?>
												</a>
											</p><p class="mb-0">
												<a class="text-primary-color">
													<?= $whatsapp ?>
												</a>
											</p>
											<p class="mb-0">
												<a class="text-link" href="mailto:<?= $email; ?>">
													<?= $email; ?>
												</a>
											</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Main Slider With Form -->
<?php endif; ?>

<!-- CAROUSEL MOBILE - SWIPE -->
<?php wp_enqueue_script('carousel-mobile', 
      SCRIPTS_THEME . 'carousel-swipe-mobile.js',  
      array('jquery-min', 'bootstrap-bundle'), false, true) ?>