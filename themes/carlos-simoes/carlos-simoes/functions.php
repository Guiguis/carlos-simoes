<?php 

/** -- Não permite que se acesse diretamente este arquivo --*/
require_once 'functions/security.php';

/*-- init --*/
add_action( 'init', 'createMenuConfiguracao' );


/*
* Criação de Options Page para RV Micologia Médica
*/
function createMenuConfiguracao(){
    if( function_exists('acf_add_options_page') ){

        $parent = acf_add_options_page(array(
            'page_title' => 'Menu de Configuração',
            'menu_slug' => 'menu-configuracao-option'
        ));

        acf_add_options_sub_page(array(
            'page_title'  => 'Informações Gerais',
            'menu_title'  => 'Informações Gerais',
            'parent_slug'   => $parent['menu_slug'],
            'menu_slug'     => 'informacoes-gerais-option'
        ));

        acf_add_options_sub_page(array(
            'page_title'  => 'Redes Sociais',
            'menu_title'  => 'Redes Sociais',
            'parent_slug'   => $parent['menu_slug'],
            'menu_slug'     => 'rede-social-option'
        ));

        acf_add_options_sub_page(array(
            'page_title'  => 'Agentes Imobiliários',
            'menu_title'  => 'Agentes Imobiliários',
            'parent_slug'   => $parent['menu_slug'],
            'menu_slug'     => 'agentes-imobiliarios'
        ));
    } 
}

/*
* Move Gravity Forms jQuery chamada para o footer
*/
function init_scripts() {
    return true;
}
add_filter("gform_init_scripts_footer", "init_scripts");



/*
* Remove Menu de Comentários
*/
function my_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );



/*
* Remove menus desnecessários
*/
function remove_menus(){

    remove_menu_page( 'edit.php' );                   //Posts
    remove_menu_page( 'edit-comments.php' );          //Comments

    // remove_menu_page( 'index.php' );                  //Dashboard
    // remove_menu_page( 'jetpack' );                    //Jetpack* 
    // remove_menu_page( 'edit.php?post_type=page' );    //Pages
    // remove_menu_page( 'themes.php' );                 //Appearance
    // remove_menu_page( 'plugins.php' );                //Plugins
    // remove_menu_page( 'users.php' );                  //Users
    // remove_menu_page( 'tools.php' );                  //Tools
    // remove_menu_page( 'options-general.php' );        //Settings

}
add_action( 'admin_menu', 'remove_menus' );



