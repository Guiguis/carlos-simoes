/*
 Theme Name: Carlos Simões
 Theme URI: https://carlosimoes.com/
 Description: Tema desenvolvido por Guilherme Miranda
 Author: Guilherme Miranda
 Author URI: http://www.gmiranda.com.br
 Version: 1.0
*/ 
$(document).ready(function() {
    "use strict";
    
	// ===========Hover Nav============	
	$('.navbar-nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});
	
});