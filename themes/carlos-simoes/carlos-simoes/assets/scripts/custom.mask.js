(function( $ ) {
	'use strict';

	$(document).ready(function($) {
		$('.mask-money').mask("#.##0,00", {reverse: true});
	});

})( jQuery );
