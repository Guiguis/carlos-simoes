<!-- Properties List -->

<?php $lstDestaques = Imoveis_Public::get_featured_posts(); ?>

<?php if(!empty($lstDestaques)): ?>
<section class="section-padding">
    <div class="section-title text-center mb-5">
        <h2>Mais Procurados</h2>
        <p>Imóveis mais procurados pelas famílias e casais.</p>
    </div>
    <div class="container">
         <div class="row justify-content-around">

            <?php $i =0; foreach ($lstDestaques as $imovel):?>
                <div class="col-lg-4 col-md-6 col-sm-6 d-flex align-items-stretch">
                    <div class="card card-list">
                        <a href="<?= $imovel['url']; ?>">
                            <span class="badge badge-info">
                                <?= $imovel['status']->name;?>
                            </span>
                            <img class="card-img-top card-img-top card-img-imoveis-inicio" 
                                 src="<?= $imovel['imagem']['url'] ?>" 
                                 alt="<?= $imovel['imagem']['alt'] ?>">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <?= $imovel['title']; ?>
                                </h5>
                                <h6 class="card-subtitle mb-2 text-muted">
                                    <i class="mdi mdi-home-map-marker"></i> 
                                   <?= $imovel['endereco']; ?>
                                </h6>
                                <h2 class="text-success mb-0 mt-3">
                                    <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                        <?php if($imovel['valor'] > 0): echo 'R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                </h2>
                            </div>
                            <div class="card-footer">
                                <span>
                                    <i class="mdi mdi-sofa"></i> 
                                    Quartos : <strong><?= $imovel['quartos']; ?></strong>
                                </span>
                                <span>
                                    <i class="mdi mdi-scale-bathroom"></i> 
                                    Banheiros : <strong><?= $imovel['banheiros']; ?></strong>
                                </span>
                                <span>
                                    <i class="mdi mdi-move-resize-variant"></i> 
                                    Área : <strong><?= $imovel['area']; ?> m²</strong>
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- End Properties List -->