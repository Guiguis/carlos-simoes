<?php

/*
 * Template Name: Sobre
 */
?>

<?php define('ROOT', dirname(__FILE__) ); ?>
<?php include ROOT . '/conf.php'; ?>

<?php get_header(); ?>

<!-- Sobre -->
<section class="section-padding">
	<div class="container">
		<div class="row">
			<div class="pl-4 col-lg-5 col-md-5 pr-4">
				<img class="rounded img-fluid" 
					 src="<?php echo IMAGES_THEME . 'casa.png' ?>"/>
			</div>
			<div class="col-lg-6 col-md-6 pl-5 pr-5">
				<h2 class="mb-4">
					Nós oferecemos uma experiência adorável no real<br> campo imobiliário</h2>
				<h5 class="mt-2">Nossa Missão</h5>
				<p>Somos uma empresa que preza pela qualidade no bem estar, conforto e segurança de nossos clientes, Agimos com transparência nas transações imobiliárias.</p>
				<h5 class="mt-4">Nossos Valores</h5>
				<p>Ética, Transparência nas transções imobiliárias, respeito pelo ser humano, respeito pela natureza e profissionalismo</p>
				<h5 class="mt-4">Nossa Visão</h5>
				<p>Alcançar o topo do mercado imobiliário de alto padrão, fidelizando nossos clientes, que são altamente exigentes</p>
			</div>
		</div>
	</div>
</section>
<!-- End Sobre -->


<!-- Agentes -->
<section class="section-padding bg-light">
	<div class="section-title text-center mb-5">
		<h2>Agentes Confiáveis</h2>
		<p>Experiência e Confiança em nossas negociações</p>
	</div>
	<div class="container">
		<div class="row justify-content-around">

			<?php
				if( get_field('agentes', 'options') ):
					while ( the_repeater_field('agentes', 'options') ): ?>
					
					<?php $nome   = get_sub_field('nome'); ?>
					<?php $imagem = get_sub_field('imagem'); ?>
					<?php $titulo = get_sub_field('titulo'); ?>
					<?php $breve_descricao = get_sub_field('breve_descricao'); ?>

					<div class="col-lg-6 col-md-6 col-sm-6 d-flex align-items-stretch">
						<div class="agents-card text-center" style="width: 100%;">
							<?php if (!empty($imagem)):?>
							<img class="img-fluid mb-4" 
								 src="<?= $imagem['url']; ?>" 
								 alt="<?= $imagem['alt']; ?>">
							<?php endif; ?>
							<p class="mb-4"><?= $breve_descricao;?></p>
							<h6 class="mb-0 text-success">- <?= $nome; ?></h6>
							<small><?= $titulo ?></small>
						</div>
					</div>

				<?php endwhile; 
			endif; ?>

		</div>
	</div>
</section>
<!-- End Agentes -->

<?php get_footer(); ?>
