<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
<head>    
	
	<!-- CONF -->
	<?php define('MAIN_THEME',    get_template_directory_uri());?>
	<?php define('SCRIPTS_THEME', get_template_directory_uri() . '/assets/scripts/');?>
	<?php define('VENDOR_THEME',  get_template_directory_uri() . '/assets/vendor/');?>
	<?php define('STYLES_THEME',  get_template_directory_uri() . '/assets/styles/');?>
	<?php define('IMAGES_THEME',  get_template_directory_uri() . '/assets/images/');?>

	<!-- METADADOS IMPORTANTES -->
	<meta charset="utf-8">
	<meta name="author"      			content="Carlos Imóveis">
	<meta name="description" 			content="Carlos Simões Imóveis">
	<meta name="viewport"    		    content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Language" content="pt-br">
	<meta http-equiv="Content-Type"     content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible"  content="IE=edge" />
	
	<!-- METADADOS PARA O SITE NO FACEBOOK -->
	<meta property="article:author" content="Carlos Simões">
	<meta property="og:site_name"   content="Carlos Simões">
	<meta property="og:description" content="Carlos Simões Imóveis">
	<meta property="og:type"        content="article">
	<meta property="og:locale"      content="en_US">
	<meta property="og:url"         content="<?php echo get_permalink(); ?>">
	<meta property="og:title"       content="<?php the_title(); ?>">

	<!-- TÍTULOS -->
	<title><?php if(empty(wp_title(''))) 'Carlos Simões'; else wp_title(''); ?></title>

	<!-- FAVICON ICON -->
	<link rel="icon" type="image/png" href="<?php echo IMAGES_THEME . 'logo/logo2.jpeg' ?>">

	<!-- BOOTSTRAAP -->
	<link href="<?php echo VENDOR_THEME . '/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">
	<link href="<?php echo VENDOR_THEME . '/select2/css/select2-bootstrap.css' ?>" rel="stylesheet">
	<link href="<?php echo VENDOR_THEME . '/select2/css/select2.min.css' ?>" rel="stylesheet">

	<!-- Material Design Icons -->
	<link href="<?php echo VENDOR_THEME . '/icons/css/materialdesignicons.min.css'?>" 
		  media="all" rel="stylesheet" type="text/css" />
	
	<!-- ÍCONES FONT-AWESOME -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" 
		  integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" 
		  crossorigin="anonymous">

	<!-- PRINCIPAL CSS -->
	<link href="<?php echo MAIN_THEME   . '/style.css'?>"  rel="stylesheet" type="text/css">
	<link href="<?php echo STYLES_THEME . 'header.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo STYLES_THEME . 'footer.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo STYLES_THEME . 'card.css' ?>"   rel="stylesheet" type="text/css">
		
	<!-- WORDPRESS HEAD -->
	<?php wp_head(); ?>
</head>
<body>
<header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand logo" href="/index.php">
				<img src="<?php echo IMAGES_THEME . 'logo/logo2.jpeg' ?>" 
					 class="d-inline-block align-top "
					 alt="Logo - Carlos Imóveis">
			</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" 
					data-toggle="collapse" data-target="#navbarResponsive" 
					aria-controls="navbarResponsive" aria-expanded="false" 
					aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0 margin-auto">
					<li class="nav-item">
						<a class="nav-link" href="/index.php">
							Home
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/imovel">
							Imóveis
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/sobre">
							Sobre
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/contato">
							Contato
						</a>
					</li>
				</ul>

				<?php 
					$email     = get_field('email', 'option'); 
					$instagram = get_field('instagram', 'option'); 
					$linkedin  = get_field('linkedin', 'option'); 
				?>

				<div class="my-2 my-lg-0">
					<ul class="list-inline main-nav-right">
						<li class="list-inline-item">
							<li class="list-inline-item">
								<a href="mailto:<?= $email; ?>" title="E-mail">
									<span class="menu-icon">
	  									<i class="fas fa-envelope"></i>
									</span>
								</a>
							</li>
							<li class="list-inline-item">
								<a href="<?= $instagram; ?>" target="_blank" title="Instagram">
									<span class="menu-icon">
		  								<i class="fab fa-instagram"></i>
									</span>
								</a>
							</li>
							<li class="list-inline-item">
								<a href="<?= $linkedin; ?>" target="_blank" title="LinkedIn">
									<span class="menu-icon">
		  								<i class="fab fa-linkedin"></i>
									</span>
								</a>
							</li>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</header>

