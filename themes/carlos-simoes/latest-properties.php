<!-- Properties List -->

<?php $lstDestaques = Imoveis_Public::get_featured_posts(); ?>

<?php if(!empty($lstDestaques)): ?>
    <section class="section-padding">
        <div class="section-title text-center mb-5">
            <h2>Mais Procurados</h2>
            <p>Imóveis mais procurados pelas famílias e casais.</p>
        </div>
        <div class="container">
           <div class="row justify-content-around">

            <?php $i=0; foreach ($lstDestaques as $imovel):?>
            <div class="col-lg-4 col-md-6 col-sm-6 d-flex align-items-stretch">
                <div class="card card-list">
                    <a href="<?= $imovel['slug']; ?>">
                        <span class="badge badge-info">
                            <?= $imovel['status']->name;?>
                        </span>                    

                        <div class="imoveis-slider pl-0 pr-0 pt-0 pb-0">
                            <div id="<?= 'imovel-' . $imovel['slug']; ?>" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php for($i = 0; $i < count($imovel['imagens_relacionadas']); $i++): ?>
                                        <li data-target="<?= '#imovel-' + $imovel['slug']; ?>" 
                                        data-slide-to="<?= $i; ?>" 
                                        class="<?php if($i == 0) echo 'active'; ?>"></li>
                                    <?php endfor; ?>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <?php $i = 0; foreach ($imovel['imagens_relacionadas'] as $imagem): ?>
                                    <div class="carousel-img-height carousel-item rounded <?php if($i == 0): echo 'active'; endif; ?>" style="background-image: url('<?= $imagem['url']; ?>')"></div>
                                    <?php $i++; endforeach; ?>
                                </div>
                                <a class="carousel-control-prev" href="<?= '#imovel-' . $imovel['slug'] ?>" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="<?= '#imovel-' . $imovel['slug']; ?>" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>

                        
                        <div class="card-body">
                            <h5 class="card-title">
                                <?= $imovel['title']; ?>
                            </h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                                <i class="mdi mdi-home-map-marker"></i> 
                                <?= $imovel['endereco']; ?>
                            </h6>

                            <?php if($imovel['isApartir']): ?>

                                <?php if($imovel['locacao']):  ?>
                                    <h6 class="text-success mb-0 mt-3">
                                        <small>a partir de</small><br>
                                        <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                        <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                        <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                        <small>/ mês</small>
                                    </h6>
                                <?php endif;  ?>

                                <?php if(!$imovel['locacao']):  ?>
                                    <h4 class="text-success mb-0 mt-3">
                                        <small>a partir de</small><br>
                                        <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                        <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                        <?php if($imovel['mensal']): ?>
                                            <small>/ mês</small>
                                        <?php endif; ?>
                                    </h4>
                                <?php endif;  ?>
                            <?php endif; ?>

                            <?php if(!$imovel['isApartir']): ?>

                                <?php if($imovel['locacao']):  ?>
                                    <h6 class="text-success mb-0 mt-3">
                                        <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                        <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                        <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                        <small>/ mês</small>
                                    </h6>
                                <?php endif;  ?>

                                <?php if(!$imovel['locacao']):  ?>
                                    <h4 class="text-success mb-0 mt-3">
                                        <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                        <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                        <?php if($imovel['mensal']): ?>
                                            <small>/ mês</small>
                                        <?php endif; ?>
                                    </h4>
                                <?php endif;  ?>
                            <?php endif; ?>
                        </div>
                        <div class="card-footer">
                            <span>
                                <i class="mdi mdi-sofa"></i> 
                                Quartos : <strong><?= $imovel['quartos']; ?></strong>
                            </span>
                            <span>
                                <i class="mdi mdi-scale-bathroom"></i> 
                                Banheiros : <strong><?= $imovel['banheiros']; ?></strong>
                            </span>
                            <span>
                                <i class="mdi mdi-move-resize-variant"></i> 
                                Área : <strong><?= $imovel['area']; ?> m²</strong>
                            </span>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>
</section>
<?php endif; ?>
<!-- End Properties List -->