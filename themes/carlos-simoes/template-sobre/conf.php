<?php



//TEMPLATE
define('PATH_DIR', get_template_directory_uri() . '/' . basename(__DIR__)  .  '/' . 'assets' . '/');
define('STYLES_DIR',  PATH_DIR . 'styles' . '/');
define('SCRIPTS_DIR', PATH_DIR . 'scripts' . '/');
define('IMAGES_DIR',  PATH_DIR . 'images' . '/');

/* enqueue template styles */
function enqueue_page_template_styles(){
	 wp_enqueue_style( 'carousel', STYLES_THEME . 'carousel.css', false, '1.1', 'all');
}

/* enqueue template scritps */
function enqueue_page_template_scripts(){}

add_action( 'wp_enqueue_scripts', 'enqueue_page_template_styles' );
add_action( 'wp_enqueue_scripts', 'enqueue_page_template_scripts' );

?>
