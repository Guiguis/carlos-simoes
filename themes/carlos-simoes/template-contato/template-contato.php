<?php

/*
 * Template Name: Contato
 */
?>

<?php define('ROOT', dirname(__FILE__) ); ?>
<?php include ROOT . '/conf.php'; ?>

<?php get_header(); ?>

<?php 
	$email     = get_field('email', 'option'); 
	$instagram = get_field('instagram', 'option'); 
	$linkedin  = get_field('linkedin', 'option');
	$whatsapp = get_field('whatsapp', 'option'); 
?>

<!-- Contact Us -->
<section class="section-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<h3 class="mt-1 mb-5">Entre em Contato!</h3>
				<h6 class="text-dark"><i class="mdi mdi-whatsapp whatsapp-icon"></i></i> WhatsApp :</h6>
				<p><?= $whatsapp; ?></p>
				<h6 class="text-dark"><i class="mdi mdi-email email-icon"></i> E-mail :</h6>
				<p><a class="text-link" href="mailto:<?= $email; ?>" title="E-mail"><?= $email; ?></a></p>
				

				<div class="footer-social"><span>Siga-nos nas Redes Sociais : </span>
					<a href="<?= $instagram; ?>" target="_blank" title="Instagram">
						<i class="mdi mdi-instagram"></i>
					</a>
					<a href="<?= $linkedin; ?>" target="_blank" title="LinkedIn">
						<i class="mdi mdi-linkedin"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-8 col-md-8">

				<!-- Contact Me -->
				<div class="row">
					<div class="col-lg-12 col-md-12 section-title text-left mb-4">
						<h2>Fale Conosco</h2>
					</div>
					<?php gravity_form(
                        'Contato', 
                        false, false, false, array(), 
                        true, 12
                    );?>
				</div>
				<!-- End Contact Me -->

			</div>
		</div>
	</div>
</section>
<!-- End Contact Us -->
 
<?php get_footer(); ?>
