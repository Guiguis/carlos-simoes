<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Imoveis
 * @subpackage Imoveis/admin
 * @author     Guilherme Miranda <gmirandatec@gmail.com>
 */
class Imoveis_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
     * Registra Custom Post Type de Imóveis
     *
     * @since    1.0.0
     */
    public function createPostTypeImovel() {
        $labels = array(
            'name'                  => _x('Imóveis', 'Post Type General Name'),
            'singular_name'         => _x('Imóvel', 'Post Type Singular Name'),
            'menu_name'             => __('Imóveis'),
            'name_admin_bar'        => __('Imóvel'),
            'all_items'             => __('Todas os Imóveis'),
            'add_new_item'          => __('Cadastrar Novo Imóvel'),
            'add_new'               => __('Cadastrar Novo'),
            'new_item'              => __('Novo Imóvel'),
            'edit_item'             => __('Editar Imóvel'),
            'update_item'           => __('Atualizar Imóvel'),
            'view_item'             => __('Ver Imóvel'),
            'view_items'            => __('Ver Imóveis'),
            'search_items'          => __('Procurar Imóvel'),
            'not_found'             => __('Nenhum Imóvel encontrado'),
            'not_found_in_trash'    => __('Nada encontrado na Lixeira'),
            'featured_image'        => __('Imagem de Destaque'),
            'set_featured_image'    => __('Escolher Imagem de Destaque'),
            'remove_featured_image' => __('Remover Imagem de Destaque'),
            'use_featured_image'    => __('Usar como Imagem de Destaque'),
            'insert_into_item'      => __('Inserir no Imóvel'),
            'uploaded_to_this_item' => __('Upload no Imóvel'),
        );

        $args = array(
            'label'                 => __('Imóveis', 'Imóveis'),
            'description'           => __('Informações dos Imóveis', 'Imóveis'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => 'dashicons-admin-home',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'rewrite' => array( 'slug' => 'imovel' ),
        );

        register_post_type('imovel', $args);
    }

    /**
     * Registra Taxonomia de Localização
     * Post Type: Imóveis
     *
     * @since    1.0.0
     */
    public function registerTaxonomyLocalizacao(){
    	$labels = array(
			'name'                       => _x( 'Localização', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Localização', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Localização', 'text_domain' ),
			'all_items'                  => __( 'Todas as Localizações', 'text_domain' ),
			'add_new_item'               => __( 'Adicionar', 'text_domain' ),
			'edit_item'                  => __( 'Editar Localização', 'text_domain' ),
			'update_item'                => __( 'Atualizar Localização', 'text_domain' ),
			'view_item'                  => __( 'Visualizar Localização', 'text_domain' )
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_in_quick_edit'         => false,
    		'meta_box_cb'                => false,
			'rewrite' => array( 'slug' => 'localizacao' )
		);

        register_taxonomy('localizacao','imovel', $args);
    }

    /**
     * Registra Taxonomia de Tipo de Imóvel
     * Post Type: Imóveis
     *
     * @since    1.0.0
     */
    public function registerTaxonomyTypeImovel(){
    	$labels = array(
			'name'                       => _x( 'Tipo', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Tipo', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Tipo', 'text_domain' ),
			'all_items'                  => __( 'Todos os Tipos', 'text_domain' ),
			'add_new_item'               => __( 'Adicionar', 'text_domain' ),
			'edit_item'                  => __( 'Editar Tipo', 'text_domain' ),
			'update_item'                => __( 'Atualizar Tipo', 'text_domain' ),
			'view_item'                  => __( 'Visualizar Tipo', 'text_domain' )
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_quick_edit'         => false,
    		'meta_box_cb'                => false,
			'rewrite' => array( 'slug' => 'tipo-imovel' )
		);

        register_taxonomy('tipo-imovel','imovel', $args);
    }

    /**
     * Registra Taxonomia de Status
     * Post Type: Imóveis
     *
     * @since    1.0.0
     */
    public function registerTaxonomyStatus(){
    	$labels = array(
			'name'                       => _x( 'Status', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Status', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Status', 'text_domain' ),
			'all_items'                  => __( 'Todos os Status', 'text_domain' ),
			'add_new_item'               => __( 'Adicionar', 'text_domain' ),
			'edit_item'                  => __( 'Editar Status', 'text_domain' ),
			'update_item'                => __( 'Atualizar Status', 'text_domain' ),
			'view_item'                  => __( 'Visualizar Status', 'text_domain' )
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_quick_edit'         => false,
    		'meta_box_cb'                => false,
			'rewrite' => array( 'slug' => 'status' )
		);

        register_taxonomy('status','imovel', $args);
    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Imoveis_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Imoveis_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/imoveis-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Imoveis_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Imoveis_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/imoveis-admin.js', array( 'jquery' ), $this->version, false );

	}

}
