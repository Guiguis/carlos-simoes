<?php

/**
 * Fired during plugin activation
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Imoveis
 * @subpackage Imoveis/includes
 * @author     Guilherme Miranda <gmirandatec@gmail.com>
 */
class Imoveis_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
