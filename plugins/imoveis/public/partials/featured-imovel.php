<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/public/partials
 */
?>


<?php $lstDestaques = Imoveis_Public::get_featured_posts(); ?>
<h5 class="card-title mb-4">Imóveis de Destaque</h5>
<div id="featured-properties" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<?php $i = 0; foreach ($lstDestaques as $imovel):?>
		<div class="carousel-item <?php if($i == 0): echo 'active'; endif; ?>">
			<div class="card card-list">
				<a href="<?= $imovel['slug']; ?>">

					<span class="badge badge-info">
						<?= $imovel['status']->name;?>
					</span>

					<img class="card-img-top" 
					src="<?= $imovel['imagem']['url']; ?>" 
					alt="<?= $imovel['imagem']['alt']; ?>">
					<div class="card-body">
						<h5 class="card-title"><?= $imovel['title']; ?></h5>
						<h6 class="card-subtitle mb-2 text-muted"><i class="mdi mdi-home-map-marker"></i><?=  $imovel['cidade']; ?></h6>

						<?php if($imovel['isApartir']): ?>

							<?php if($imovel['locacao']):  ?>
								<h6 class="text-success mb-0 mt-3">
									<small>a partir de</small><br>
									<?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
									<?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
									<?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
									<small>/ mês</small>
								</h6>
							<?php endif;  ?>

							<?php if(!$imovel['locacao']):  ?>
								<h4 class="text-success mb-0 mt-3">
									<small>a partir de</small><br>
									<?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
									<?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
									<?php if($imovel['mensal']): ?>
										<small>/ mês</small>
									<?php endif; ?>
								</h4>
							<?php endif;  ?>
						<?php endif; ?>

						<?php if(!$imovel['isApartir']): ?>

							<?php if($imovel['locacao']):  ?>
								<h6 class="text-success mb-0 mt-3">
									<?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
									<?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
									<?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
									<small>/ mês</small>
								</h6>
							<?php endif;  ?>

							<?php if(!$imovel['locacao']):  ?>
								<h4 class="text-success mb-0 mt-3">
									<?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
									<?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
									<?php if($imovel['mensal']): ?>
										<small>/ mês</small>
									<?php endif; ?>
								</h4>
							<?php endif;  ?>
						<?php endif; ?>

					</div>
				</a>
			</div>
		</div>
		<?php $i++; endforeach; ?>
	</div>
</div>