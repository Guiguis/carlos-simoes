<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/public/partials
 */
?>

<?php get_header(); ?>

<?php 

$localizacao_get = $_GET["localizacao"];
$status_get      = $_GET["status"];
$tipo_get        = $_GET["tipo"];
$quartos_get     = $_GET["quartos"];
$banheiros_get   = $_GET["banheiros"];
$valor_max       = $_GET["valor_max"];
$valor_min       = $_GET["valor_min"];

$imoveis = Imoveis_Public::get_imoveis(
    $localizacao_get, 
    $status_get, 
    $tipo_get, 
    $quartos_get, 
    $banheiros_get,
    $valor_min,
    $valor_max
);

?>

<!-- Main Slider With Form -->
<?php $imagens = get_field('imagens_imoveis', 'option'); ?>

<section class="imoveis-slider slider-h-auto">


    <div id="osahanslider" class="carousel slide" data-ride="carousel">
        <?php if( have_rows('imagens_imoveis', 'option') ): ?>
            <ol class="carousel-indicators">
                <?php $i = 0; while ( have_rows('imagens_imoveis', 'option') ) : the_row(); ?>
                <li data-target="#osahanslider" data-slide-to="<?= $i; ?>" 
                    class="<?php if($i == 0) : echo 'active'; endif;  ?>"></li>
                    <?php $i++; endwhile; ?>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <?php $i = 0; while ( have_rows('imagens_imoveis', 'option') ) : the_row(); ?>
                    <div class="carousel-item arousel-item-background 
                    <?php if($i == 0) : echo 'active'; endif;?>" 
                    style="background-image: url('<?= get_sub_field('imagem')['url']; ?>')">
                    <div class="overlay"></div>
                </div>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="slider-form inner-page-form">
        <div class="container">
            <h1 class="text-center text-white mb-5">Encontre seu Imóvel Favorito</h1>
            <form method="get">
                <div class="row no-gutters">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="mdi mdi-map-marker-multiple"></i>
                            </div>
                            <?php $localizacao = Imoveis_Public::get_terms_taxonomy('localizacao'); ?>
                            <select class="form-control select2" name="localizacao">
                                <option value="" disabled>Localização</option>
                                <option <?php if($localizacao_get == '') : echo 'selected'; endif; ?> 
                                value="">Todos</option>
                                <?php foreach ($localizacao as $local): ?>
                                    <option <?php if($localizacao_get == $local->slug) : echo 'selected'; endif; ?> value="<?= $local->slug ?>"><?= $local->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="mdi mdi-city"></i>
                            </div>
                            <?php $status = Imoveis_Public::get_terms_taxonomy('status'); ?>
                            <select class="form-control select2" name="status">
                                <option value="" disabled>Status</option>
                                <option <?php if($status_get == '') : echo 'selected'; endif; ?> value="">Todos</option>
                                <?php foreach ($status as $status): ?>
                                    <option <?php if($status_get == $status->slug) : echo 'selected'; endif; ?> value="<?= $status->slug ?>"><?= $status->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="mdi mdi-home-modern"></i>
                            </div>
                            <?php $tipos = Imoveis_Public::get_terms_taxonomy('tipo-imovel'); ?>
                            <select class="form-control select2" name="tipo">
                                <option value="" disabled>Tipo</option>
                                <option <?php if($tipo_get == '') : echo 'selected'; endif; ?> value="">Todos</option>
                                <?php foreach ($tipos as $tipo): ?>
                                    <option <?php if($tipo_get == $tipo->slug) : echo 'selected'; endif; ?> value="<?= $tipo->slug ?>"><?= $tipo->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="mdi mdi-hotel"></i>
                            </div>
                            <select class="form-control select2" name="quartos">
                                <option <?php if($quartos_get == '') : echo 'selected'; endif; ?> 
                                value="">
                                Quartos
                            </option>
                            <?php for($i=1; $i <= 9; $i++): ?>
                                <option <?php if($quartos_get == $i) : echo 'selected'; endif; ?>>
                                    <?= $i ?>
                                </option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fas fa-bath"></i>
                        </div>
                        <select class="form-control select2" name="banheiros">
                            <option <?php if($banheiros_get == '') : echo 'selected'; endif; ?> 
                            value="">Banheiros</option>
                        </option>
                        <?php for($i=1; $i <= 9; $i++): ?>
                            <option <?php if($banheiros_get == $i) : echo 'selected'; endif; ?>>
                                <?= $i ?>
                            </option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <input class="form-control mask-money" type="text" name="valor_min" 
                    placeholder="Valor Mínimo" value="<?= $valor_min ?>">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <input class="form-control mask-money" type="text" name="valor_max"
                    placeholder="Valor Máximo" value="<?= $valor_max ?>">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="input-group">
                    <button type="submit" class="btn btn-main primary-color btn-block no-radius font-weight-bold ">
                        PROCURAR
                    </button>   
                </div>
            </div>
        </div>
    </form>
</div>
</div>
</section>
<!-- End Main Slider With Form -->

<!-- Properties List -->
<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mb-3">Tipo de Imóvel</h5>
                        <ul class="sidebar-card-list">
                            <?php $tipos = Imoveis_Public::get_terms_taxonomy('tipo-imovel');  ?>
                            <?php foreach ($tipos as $tipo): ?>
                                <?php if($tipo->count > 0): $link = '?tipo=' . $tipo->slug; ?>
                                    <li>
                                        <a href="<?= $link; ?>">
                                            <i class="mdi mdi-chevron-right"></i> <?= $tipo->name ?>  
                                            <span class="sidebar-badge"><?= $tipo->count ?></span></a>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title mb-3">Localização</h5>
                                <ul class="sidebar-card-list">
                                    <?php $lstLocalizacao = Imoveis_Public::get_terms_taxonomy('localizacao');  ?>
                                    <?php foreach ($lstLocalizacao as $localizacao): ?>
                                        <?php if($localizacao->count > 0): $link = '?localizacao=' . $localizacao->slug; ?>
                                            <li>
                                                <a href="<?= $link; ?>">
                                                    <i class="mdi mdi-chevron-right"></i> <?= $localizacao->name ?>  
                                                    <span class="sidebar-badge"><?= $localizacao->count ?></span></a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            
                            <!-- DESTAQUE -->
                            <div class="card hidden-sm-down">
                                <div class="card-body">
                                    <?php include 'featured-imovel.php'  ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="row">
                                <?php if(!empty($imoveis['posts'])): foreach ($imoveis['posts'] as $imovel): ?>

                                    <!-- CASA -->
                                    <?php if($imovel['tipo']->slug == 'casa' || $imovel['tipo']->slug != 'lote'): ?>
                                        <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
                                            <div class="card card-list">
                                                <a href="<?= $imovel['slug']; ?>">
                                                <span class="badge badge-info">
                                                    <?= $imovel['status']->name;?>
                                                </span>

                                                <div class="imoveis-slider pl-0 pr-0 pt-0 pb-0">
                                                    <div id="<?= 'imovel-' . $imovel['slug']; ?>" class="carousel slide" data-ride="carousel">
                                                        <ol class="carousel-indicators">
                                                            <?php for($i = 0; $i < count($imovel['imagens_relacionadas']); $i++): ?>
                                                                <li data-target="<?= '#imovel-' + $imovel['slug']; ?>" 
                                                                data-slide-to="<?= $i; ?>" 
                                                                class="<?php if($i == 0) echo 'active'; ?>"></li>
                                                            <?php endfor; ?>
                                                        </ol>
                                                        <div class="carousel-inner" role="listbox">
                                                            <?php $i = 0; foreach ($imovel['imagens_relacionadas'] as $imagem): ?>
                                                            <div class="carousel-img-height carousel-item rounded <?php if($i == 0): echo 'active'; endif; ?>" style="background-image: url('<?= $imagem['url']; ?>')"></div>
                                                            <?php $i++; endforeach; ?>
                                                        </div>
                                                        <a class="carousel-control-prev" href="<?= '#imovel-' . $imovel['slug'] ?>" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="<?= '#imovel-' . $imovel['slug']; ?>" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="card-body">
                                                    <h5 class="card-title">
                                                        <?= $imovel['title'];  ?>
                                                    </h5>
                                                    <h6 class="card-subtitle mb-2 text-muted">
                                                        <i class="mdi mdi-home-map-marker"></i> 
                                                        <?= $imovel['cidade']; ?>
                                                    </h6>

                                                    <?php if($imovel['isApartir']): ?>

                                                        <?php if($imovel['locacao']):  ?>
                                                            <h6 class="text-success mb-0 mt-3">
                                                                <small>a partir de</small><br>
                                                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                                <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                                                <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                                                <small>/ mês</small>
                                                            </h6>
                                                        <?php endif;  ?>
                                                        
                                                        <?php if(!$imovel['locacao']):  ?>
                                                            <h2 class="text-success mb-0 mt-3">
                                                                <small>a partir de</small><br>
                                                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                                <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                                                <?php if($imovel['mensal']): ?>
                                                                    <small>/ mês</small>
                                                                <?php endif; ?>
                                                            </h2>
                                                        <?php endif;  ?>
                                                    <?php endif; ?>

                                                    <?php if(!$imovel['isApartir']): ?>

                                                        <?php if($imovel['locacao']):  ?>
                                                            <h6 class="text-success mb-0 mt-3">
                                                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                                <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                                                <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                                                <small>/ mês</small>
                                                            </h6>
                                                        <?php endif;  ?>

                                                        <?php if(!$imovel['locacao']):  ?>
                                                            <h2 class="text-success mb-0 mt-3">
                                                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                                <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                                                <?php if($imovel['mensal']): ?>
                                                                    <small>/ mês</small>
                                                                <?php endif; ?>
                                                            </h2>
                                                        <?php endif;  ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="card-footer">
                                                    <span>
                                                        <i class="mdi mdi-sofa"></i> 
                                                        Quartos : <strong><?= $imovel['quartos']; ?></strong>
                                                    </span>
                                                    <span>
                                                        <i class="fas fa-bath"></i>
                                                        Banheiros : <strong><?= $imovel['banheiros']; ?></strong>
                                                    </span>
                                                    <span>
                                                        <i class="mdi mdi-move-resize-variant"></i> 
                                                        Área : <strong><?= $imovel['area']; ?> m²</strong>
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!-- LOTE -->
                                <?php if($imovel['tipo']->slug == 'lote'): ?>
                                    <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
                                        <div class="card card-list">
                                            <a href="<?= $imovel['slug']; ?>">
                                               <span class="badge badge-info">
                                                <?= $imovel['status']->name;?>
                                            </span>

                                            <img class="card-img-top card-img-imoveis" 
                                            src="<?= $imovel['imagem']['url']; ?>" 
                                            alt="<?php $imovel['imagem']['alt']; ?>">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    <?= $imovel['title'];  ?>
                                                </h5>
                                                <h6 class="card-subtitle mb-2 text-muted">
                                                    <i class="mdi mdi-home-map-marker"></i> 
                                                    <?= $imovel['cidade']; ?>
                                                </h6>
                                                
                                                <?php if($imovel['isApartir']): ?>

                                                    <?php if($imovel['locacao']):  ?>
                                                        <h6 class="text-success mb-0 mt-3">
                                                            <small>a partir de</small><br>
                                                            <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                            <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                                            <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                                            <small>/ mês</small>
                                                        </h6>
                                                    <?php endif;  ?>
                                                    
                                                    <?php if(!$imovel['locacao']):  ?>
                                                        <h2 class="text-success mb-0 mt-3">
                                                            <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                            <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                                            <?php if($imovel['mensal']): ?>
                                                                <small>/ mês</small>
                                                            <?php endif; ?>
                                                        </h2>
                                                    <?php endif;  ?>
                                                <?php endif; ?>

                                                <?php if(!$imovel['isApartir']): ?>

                                                    <?php if($imovel['locacao']):  ?>
                                                        <h6 class="text-success mb-0 mt-3">
                                                            <small>a partir de</small><br>
                                                            <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                            <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                                            <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                                            <small>/ mês</small>
                                                        </h6>
                                                    <?php endif;  ?>

                                                    <?php if(!$imovel['locacao']):  ?>
                                                        <h2 class="text-success mb-0 mt-3">
                                                            <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                                            <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                                            <?php if($imovel['mensal']): ?>
                                                                <small>/ mês</small>
                                                            <?php endif; ?>
                                                        </h2>
                                                    <?php endif;  ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="card-footer">
                                                <span>
                                                    <i class="mdi mdi-move-resize-variant"></i> 
                                                    Área : <strong><?= $imovel['area']; ?> m²</strong>
                                                </span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php endif; ?>

                        <?php endforeach; endif; ?>             
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Properties List -->

    <!-- CAROUSEL MOBILE - SWIPE -->
    <?php wp_enqueue_script('carousel-mobile', 
      SCRIPTS_THEME . 'carousel-swipe-mobile.js',  
      array('jquery-min', 'bootstrap-bundle'), false, true) ?>

    <?php wp_enqueue_script('mask-jquery', 
      SCRIPTS_THEME . 'jquery.mask.js',  
      array('jquery-min', 'bootstrap-bundle'), false, true) ?>

    <?php wp_enqueue_script('custom-mask', 
      SCRIPTS_THEME . 'custom.mask.js',  
      array('jquery-min', 'bootstrap-bundle', 'mask-jquery'), false, true) ?>

      <?php get_footer(); ?>
