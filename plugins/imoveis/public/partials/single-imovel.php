<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/public/partials
 */
?>

<!-- IMÓVEL -->
<?php
global $post;     
$imovel = Imoveis_Public::get_imovel($post->ID);
?>

<?php get_header(); ?>

<!-- Property Single Slider header -->
<section class="imoveis-slider">
    <div id="osahanslider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active" 
            style="background-image: url('<?= $imovel['imagem']['url']; ?>')"></div>
        </div>
    </div>
    <div class="property-single-title property-single-title-gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <h1><?= $imovel['title'] ?></h1>
                    <h6>
                        <i class="mdi mdi-home-map-marker"></i> 
                        <?= $imovel['cidade'] ?>
                    </h6>
                </div>
                <div class="col-lg-4 col-md-4 text-right">
                    <h6 class="mt-2"><?= $imovel['status']->name; ?></h6>

                    <?php if($imovel['isApartir']): ?>

                        <?php if($imovel['locacao']):  ?>
                            <h5 class="text-success mb-0 mt-3">
                                <small>a partir de</small><br>
                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                <small>/ mês</small>
                            </h5>
                        <?php endif;  ?>

                        <?php if(!$imovel['locacao']):  ?>
                            <h2 class="text-success mb-0 mt-3">
                                <small>a partir de</small><br>
                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                <?php if($imovel['mensal']): ?>
                                    <small>/ mês</small>
                                <?php endif; ?>
                            </h2>
                        <?php endif;  ?>
                    <?php endif; ?>

                    <?php if(!$imovel['isApartir']): ?>

                        <?php if($imovel['locacao']):  ?>
                            <h5 class="text-success mb-0 mt-3">
                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                <?php if($imovel['valor'] > 0): echo 'Venda: R$' . number_format($imovel['valor'], 2, ',', '.'); endif; ?><br/>
                                <?php if($imovel['valor_locacao'] > 0): echo 'Locação: R$' . number_format($imovel['valor_locacao'], 2, ',', '.'); endif; ?>
                                <small>/ mês</small>
                            </h5>
                        <?php endif;  ?>

                        <?php if(!$imovel['locacao']):  ?>
                            <h2 class="text-success mb-0 mt-3">
                                <?php if($imovel['valor'] == 0): echo 'Sob consulta'; endif; ?>
                                <?php if($imovel['valor'] > 0): echo 'R$ ' . number_format($imovel['valor'], 2, ',', '.'); endif; ?>
                                <?php if($imovel['mensal']): ?>
                                    <small>/ mês</small>
                                <?php endif; ?>
                            </h2>
                        <?php endif;  ?>
                    <?php endif; ?>
                </div>
            </div>
            <hr>
        </div>
    </div>
</section>
<!-- End Property Single Slider header -->

<!-- Property Single Slider -->
<section class="section-padding-imovel">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="card">
                    <div class="card-body imoveis-slider pl-0 pr-0 pt-0 pb-0">
                        <div id="imovelslider" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php for($i = 0; $i < count($imovel['imagens_relacionadas']); $i++): ?>
                                    <li data-target="#imovelslider" 
                                    data-slide-to="<?= $i; ?>" 
                                    class="<?php if($i == 0) echo 'active'; ?>"></li>
                                <?php endfor; ?>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <?php $i = 0; foreach ($imovel['imagens_relacionadas'] as $imagem): ?>
                                <div class="carousel-item rounded <?php if($i == 0): echo 'active'; endif; ?>" style="background-image: url('<?= $imagem['url']; ?>')"></div>
                                <?php $i++; endforeach; ?>
                            </div>
                            <a class="carousel-control-prev" href="#imovelslider" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#imovelslider" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

                <?php if($imovel['tipo']->slug == 'casa' || $imovel['tipo']->slug != 'lote'): ?>
                    <div class="card padding-card">
                        <div class="card-body">
                            <h5 class="card-title mb-3">Descrição</h5>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-move-resize-variant"></i>
                                        <strong>Área:</strong>
                                        <p class="mb-0"><?= $imovel['area'] ?> m²</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-sofa"></i>
                                        <strong>Quartos</strong>
                                        <p class="mb-0"><?= $imovel['quartos'] ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-hot-tub"></i>
                                        <strong>Banheiros</strong>
                                        <p class="mb-0"><?= $imovel['banheiros'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-floor-plan"></i>
                                        <strong>Andares:</strong>
                                        <p class="mb-0"><?= $imovel['andares'] ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-car-convertible"></i>
                                        <strong>Garagem:</strong>
                                        <p class="mb-0"><?= $imovel['carros_garagem'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <p class="text-justify">
                                <?= $imovel['descricao']; ?>
                            </p>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if($imovel['tipo']->slug == 'lote'): ?>
                    <div class="card padding-card">
                        <div class="card-body">
                            <h5 class="card-title mb-3">Descrição</h5>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="list-icon">
                                        <i class="mdi mdi-move-resize-variant"></i>
                                        <strong>Área:</strong>
                                        <p class="mb-0"><?= $imovel['area'] ?> m²</p>
                                    </div>
                                </div>
                            </div>
                            <p class="text-justify">
                                <?= $imovel['descricao']; ?>
                            </p>
                        </div>
                    </div>
                <?php endif; ?>
                
                <!-- LOCALIZAÇÃO -->
                <div class="card padding-card">
                    <div class="card-body">
                        <h5 class="card-title mb-3">Localização</h5>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p>
                                    <strong class="text-dark">Localização :</strong>
                                    <?= $imovel['cidade'] ?>
                                </p>                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- VIDEO -->
                <?php if(!empty($imovel['video'])): ?>
                    <div class="card padding-card">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Conheça o Imóvel</h5>
                            <?= $imovel['video'] ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-lg-4 col-md-4">
                <div class="card sidebar-card">
                    <div class="card-body">
                        <h5 class="card-title mb-4">Solicitar Visita</h5>
                        <?php gravity_form(
                            'Solicitação de Visita', 
                            false, false, false, 
                            array('nome_imovel' => $imovel['title'], 'link_imovel' => get_permalink()), 
                            true, 12
                        );?>
                    </div>
                </div>

                <!-- DESTAQUE -->
                <div class="card sidebar-card">
                    <div class="card-body">
                        <?php include 'featured-imovel.php'  ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- CAROUSEL MOBILE - SWIPE -->
<?php wp_enqueue_script('carousel-mobile', 
  SCRIPTS_THEME . 'carousel-swipe-mobile.js',  
  array('jquery-min', 'bootstrap-bundle'), false, true) ?>

  <?php get_footer(); ?>

  <!-- This file should primarily consist of HTML with a little bit of PHP. -->
