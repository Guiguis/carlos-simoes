<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Imoveis
 * @subpackage Imoveis/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Imoveis
 * @subpackage Imoveis/public
 * @author     Guilherme Miranda <gmirandatec@gmail.com>
 */
class Imoveis_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Cria pages templates do plugin vitrine serasa
	 *
	 * @since    1.0.0
	 */
	public function createPageTemplate($page_template) {

		//Verifica slug da url da página
		if ( is_page( 'imovel' ) ) {
			$page_template = dirname(__FILE__) . '/partials/index-imovel.php';
		}

		//Retorna a página a ser criada
		return $page_template;
	}

	/**
	 * Cria as singles dos Custom Post Types Imovel
	 *
	 * @since    1.0.0
	 */
	public function createCustomPostSingles($single_template) {
		global $post;

		if ($post->post_type === 'imovel') {
			$single_template = dirname(__FILE__) . '/partials/single-imovel.php';
		}

		return $single_template;
	}

	/**
	 * Retorna todas os termos das taxonomias 
	 * @param $taxonomy string tipo de taxonomia a ser filtrada
	 * @return Termos das Taxonomias
	 */
	public static function get_terms_taxonomy($taxonomy) {
		return get_terms( array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
		));
	}

	/**
	 * Retorna os imóveis cadastrados com diversos filtros
	 * @param  object $post Post do Imóvel
	 * @return array Array associativo com os dados dos cursos carregados.
	 */
	public static function get_imovel($id){
		$post = get_post($id);

		$imagem 	    = get_field('imagem', $id);
		$valor 		    = get_field('valor', $id);
		$endereco 	    = get_field('endereco', $id);
		$area     	    = get_field('area', $id);
		$quartos 	    = get_field('quartos', $id);
		$banheiros      = get_field('banheiros', $id);
		$descricao      = get_field('descricao', $id);
		$andares        = get_field('andares', $id);
		$carros_garagem = get_field('carros_garagem', $id);
		$video   	    = get_field('video', $id);
		$agente_nome    = get_field('nome', $id);
		$agente_email   = get_field('email', $id);
		$agente_celular = get_field('celular', $id);
		$isApartir   	= get_field('isApartir', $id);
		$isMensal   	= get_field('mensal', $id);
		$isLocacao   	= get_field('locacao', $id);
		$valor_locacao  = get_field('valor_locacao', $id);

		if($isMensal == 0) $isMensal = false;
		if($isMensal == 1) $isMensal = true;

		if($isLocacao == 1) $isLocacao = true;
		if($isLocacao == 1) $isLocacao = true;

		//Imagens Relacionadas
		$imagens_relacionadas = array();
		if(have_rows('imagens_relacionadas', $id)){
			foreach (get_field('imagens_relacionadas', $id) as $key => $value) {
				array_push($imagens_relacionadas, $value['imagem']);
			}
			
		}

		//Adiciona termos do post ao array - Status
		$terms_status = wp_get_post_terms($id, array('status'));
		$terms_cidade = wp_get_post_terms($id, array('localizacao'));
		$terms_tipo   = wp_get_post_terms($id, array('tipo-imovel'));

		$imovel = array(
			'id' 	   	=> $id,
			'title'    	=> $post->post_title,
			'slug' 	   	=> $post->post_name,
			'imagem' 	=> $imagem,
			'valor' 	=> $valor,
			'endereco'  => $endereco,
			'area' 	    => $area,
			'quartos'   => $quartos,
			'banheiros' => $banheiros,
			'andares'   => $andares,
			'video'     => $video,
			'status'    => $terms_status[0],
			'tipo'      => $terms_tipo[0],
			'cidade'    => $terms_cidade[0]->name,
			'descricao' => $descricao,
			'mensal' 	=> $isMensal,
			'locacao' 	=> $isLocacao,
			'isApartir' => $isApartir,
			'valor_locacao'  => $valor_locacao,
			'agente_nome'    => $agente_nome,
			'agente_email'   => $agente_email,
			'agente_celular' => $agente_celular,
			'carros_garagem'       => $carros_garagem,
			'imagens_relacionadas' => $imagens_relacionadas
		);

		return $imovel;
	}

	/**
	 * Retorna os imóveis cadastrados com diversos filtros
	 * @param string  $localizacao Localização do Imóvel
	 * @param string  $status Status do Imóvel
	 * @param string  $tipo Tipo do Imóvel
	 * @param integer $quartos Número de Quartos
	 * @param integer $banheiros Número de Banheiros
	 * @param integer $valor_min Valor Mínimo do Imóvel
	 * @param integer $valor_max Valor Máximo do Imóvel
	 * @return array Array associativo com os dados dos cursos carregados.
	 */
	public static function get_imoveis($localizacao = '', $status = '', $tipo = '', $quartos = 0, 
									   $banheiros = 0, $valor_min = 0, $valor_max = 0)
	{	

		$valor_min = Imoveis_Public::get_value_imovel($valor_min); 
		$valor_max = Imoveis_Public::get_value_imovel($valor_max); 

		if($valor_max == 0 || empty($valor_max)) $valor_max = 9999999999999999999999999999;

		$args = array(
			'post_type' => 'imovel',
			'posts_per_page'   => -1,
			'post_status' => 'publish',
			'meta_key' => 'valor',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
		);


		//Filtra por localização
		$tax_query_localizacao = array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'localizacao',
				'field' => 'slug',
				'terms' => $localizacao,
			)
		);
	
		//Filtra por Status
		$tax_query_status = array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'status',
				'field' => 'slug',
				'terms' => $status,
			)
		);

		//Filtra por Tipo
		$tax_query_tipo = array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'tipo-imovel',
				'field' => 'slug',
				'terms' => $tipo,
			)
		);

		//Quartos
		$meta_query_quartos = array( 
			'relation'		=> 'OR',
			array(
				'key'		=> 'quartos',
				'value'		=> $quartos,
				'compare'	=> '>='
			)
		);

		//Banheiros
		$meta_query_banheiros = array( 
			'relation'		=> 'OR',
			array(
				'key'		=> 'banheiros',
				'value'		=> $banheiros,
				'compare'	=> '>='
			)
		);

		//Valor
		$meta_valor = array( 
			'relation'		=> 'AND',
			array(
				'key' => 'valor',
				'value' => array( $valor_min, $valor_max ),
				'type' => 'numeric',
				'compare' => 'BETWEEN'
			)
		);

		if(!empty($localizacao)) $args['tax_query'][]  = $tax_query_localizacao;
		if(!empty($status))      $args['tax_query'][]  = $tax_query_status;
		if(!empty($tipo))        $args['tax_query'][]  = $tax_query_tipo;
		if($quartos   != 0)      $args['meta_query'][] = $meta_query_quartos;
		if($banheiros != 0)      $args['meta_query'][] = $meta_query_banheiros;
		if($valor_min != 0 || $valor_min != 0)      $args['meta_query'][] = $meta_valor;

		$query_imoveis = new WP_Query($args);

		$imoveis = array();
		foreach ($query_imoveis->posts as $imovel) {
			$imoveis['posts'][] = Imoveis_Public::get_imovel($imovel->ID); 
		}

		wp_reset_postdata();
		return $imoveis;
	}

	/**
	* Retorna todos os posts por taxonomia desejada
	* @param $taxonomy string Tipo de taxonomia a ser filtrada
	* @return Posts
	*/
	public static function get_featured_posts(){

		$args = array(
			'post_type' => 'imovel',
			'posts_per_page'   => -1,
			'post_status' => 'publish',
			'meta_query' => array(
				'relation'		=> 'OR',
				array(
					'key'		=> 'destaque',
					'value'		=> true,
					'compare'	=> '='
				),
			)
		);

		$wp_query = new WP_Query($args);

		$imoveis = array();
		foreach ($wp_query->posts as $imovel) {
			$imovelId = $imovel->ID;
			
			$imoveis[] = Imoveis_Public::get_imovel($imovelId);
		}
		wp_reset_postdata();

		return $imoveis;
	}

	/**
	 * Retorna valor da moeda brasileiro convertido
	 * @return number com o valor do imóvel.
	 */
	public static function get_value_imovel($brazilian_number){
		$number = floatval(str_replace(',', '.', str_replace('.', '', $brazilian_number)));
		return $number;
	}

	/**
	 * Retorna os imóveis cadastrados que aparecerão no carousel
	 * @return array Array associativo com os dados dos imóveis.
	 */
	public static function get_imoveis_carousel(){
		$args = array(
			'post_type' => 'imovel',
			'posts_per_page'   => -1,
			'post_status' => 'publish',
			'meta_query' => array(
				'relation'		=> 'OR',
				array(
					'key'		=> 'exibir_slider',
					'value'		=> true,
					'compare'	=> '='
				),
			),
		);

		$query_imoveis = new WP_Query($args);

		$imoveis = array();
		foreach ($query_imoveis->posts as $imovel) {
			$imovelId = $imovel->ID;

			$imoveis['posts'][] = Imoveis_Public::get_imovel($imovelId);
		}

		wp_reset_postdata();
		return $imoveis;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Imoveis_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Imoveis_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/imoveis-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Imoveis_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Imoveis_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/imoveis-public.js', array( 'jquery' ), $this->version, false );

	}

}